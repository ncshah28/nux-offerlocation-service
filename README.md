# Template Microservice

## Developer Notes

### Lombok
This project uses lombok to dynamically generate boilerplate bean properties

* Setup for Lombok is provided to Gradle via the _gradle-lombok_ plugin
  * No further steps are needed by the developer
* Setup for IDE:
  * To install the Lombok plugin for IntelliJ follow these steps.
    - Go to File > Settings > Plugins
    - Click on Browse repositories...
    - Search for Lombok Plugin
    - Click on Install plugin
    - Restart IntelliJ IDEA   
    - For more details refer to - https://projectlombok.org/setup/intellij
  * To install the Lombok plugin for Eclipse follow these steps  
    - run the following
    ```bash
    $ ./gradlew installLombok
    ```    

### Building the micro-service
*NOTE:* Before proceeding dependencies should have been built and published. For example, 
  * JsonAPI 
  
```bash
$ ./gradlew build
```



