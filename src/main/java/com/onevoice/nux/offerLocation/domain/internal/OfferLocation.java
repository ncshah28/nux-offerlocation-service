package com.onevoice.nux.offerLocation.domain.internal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Created by nidhis on 12/04/2018.
 */
@Data
@Component

public class OfferLocation {

    public static final String RESOURCE_TYPE = "x-nux-offer-location-v1.0.0";
    Long locationId;

    BigDecimal latitude;

    BigDecimal  longitude;

    String offerId;

    String offerName;

    Double distance;
}
