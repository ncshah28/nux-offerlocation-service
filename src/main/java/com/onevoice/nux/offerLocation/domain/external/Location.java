package com.onevoice.nux.offerLocation.domain.external;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Created by nidhis on 11/04/2018.
 */

@Data
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Location {
    @JsonIgnore
    private Long id;

    private BigDecimal latitude;

    private BigDecimal longitude;

    public Location() {
    }

    public Location(BigDecimal latitude, BigDecimal longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
