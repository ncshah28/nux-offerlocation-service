/*
 * Copyright (c) 2018. OneVoice / Arrk Group
 * All rights reserved
 */
package com.onevoice.nux.offerLocation.domain.external;

public final class ErrorCodes {
    public static final String NOT_FOUND = "E_1004";
    public static final String NO_CONTENT = "E_2004";
    public static final String GENERAL_ERROR = "E_1001";
    public static final String TECHNICAL_ERROR = "E_1002";
    public static final String PERMISSION_ERROR = "E_3001";
    public static final String PERMISSION_BAD_PARAMS = "E_2010";
    public static final String INVALID_DATA = "E_7001";
    private ErrorCodes() {
    }
}
