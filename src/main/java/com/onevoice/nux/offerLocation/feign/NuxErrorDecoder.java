/*
 * Copyright (c) 2018. OneVoice / Arrk Group
 * All rights reserved
 */

package com.onevoice.nux.offerLocation.feign;

import com.onevoice.nux.offerLocation.exception.ServiceException;
import feign.Response;
import feign.codec.ErrorDecoder;

import static feign.FeignException.errorStatus;

public class NuxErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() >= 400) {
            return new ServiceException(response.status(), response.reason());
        }
        return errorStatus(methodKey, response);
    }
}