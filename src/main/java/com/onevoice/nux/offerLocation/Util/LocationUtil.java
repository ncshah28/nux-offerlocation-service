package com.onevoice.nux.offerLocation.Util;

import com.onevoice.nux.offerLocation.domain.internal.OfferLocation;

import java.math.BigDecimal;


/**
 * Created by nidhis on 11/04/2018.
 */
public class LocationUtil {




    public static OfferLocation calculateDistance(OfferLocation offer_location, BigDecimal usersLat, BigDecimal usersLong, String unit) {
        Double offerLat=offer_location.getLatitude().doubleValue();
        Double offerLon=offer_location.getLongitude().doubleValue();
        Double userLat= usersLat.doubleValue();
        Double userLong=usersLong.doubleValue();
        double theta = userLong - offerLon;
        double dist = Math.sin(deg2rad(userLat)) * Math.sin(deg2rad(offerLat)) + Math.cos(deg2rad(userLat)) * Math.cos(deg2rad(offerLat)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344;
        } else if (unit == "N") {
            dist = dist * 0.8684;
        }
        offer_location.setDistance(dist);
        return offer_location;
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts decimal degrees to radians						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts radians to decimal degrees						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public static OfferLocation min(OfferLocation a, OfferLocation b) {
        return a.getDistance() < b.getDistance() ? a : b;
    }


}
