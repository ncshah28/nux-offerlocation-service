/*
 * Copyright (c) 2018. OneVoice / Arrk Group
 * All rights reserved
 */

package com.onevoice.nux.offerLocation.controller;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class NuxResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {



    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleGenericException(Exception exception, WebRequest request) {
        String explanation = "Could not fetch the Offer location";
        return handleExceptionInternal(exception, explanation,
                new HttpHeaders(), HttpStatus.CONFLICT, request);
    }




}