/*
 * Copyright (c) 2018. OneVoice / Arrk Group
 * All rights reserved
 */
package com.onevoice.nux.offerLocation.controller;

import com.onevoice.commons.api.JsonApiEnvelope;

import com.onevoice.nux.offerLocation.domain.external.*;

import com.onevoice.nux.offerLocation.domain.internal.OfferLocation;
import com.onevoice.nux.offerLocation.service.OfferLocationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
public class OfferLocationController {

    private static Logger log = LoggerFactory.getLogger(OfferLocationController.class);



    OfferLocationService offerLocationService;

    public OfferLocationController(OfferLocationService offerLocationService) {
        this.offerLocationService = offerLocationService;
    }



    @GetMapping("/offer-sites")
    public ResponseEntity<JsonApiEnvelope> findOfferLocations( @RequestParam("offer-id")  String offerId, @RequestParam("reference-lat")  float referenceLat, @RequestParam("reference-long") final double referenceLong)
    {

        log.debug("Handle  \"/offerLocation\" : Get");
        JsonApiEnvelope offerLocationEnvelope = new JsonApiEnvelope();

        if( offerLocationService.getListOfFilteredOffers(offerId).isEmpty())
        {
            String errDetail = "The Offer does not exist or is not valid";

            offerLocationEnvelope.error(ErrorCodes.NOT_FOUND, "The Offer does not exist or is not valid", errDetail);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(offerLocationEnvelope);
        }

                List<OfferLocation> offerLocation = offerLocationService.getListOfNearestLocation(offerId, new BigDecimal(referenceLat), new BigDecimal(referenceLong));

                if (offerLocation != null && !offerLocation.isEmpty()) {
                    String finalOfferId = offerLocation.get(0).getOfferId();
                    BigDecimal latitude = offerLocation.get(0).getLatitude();
                    BigDecimal longitude = offerLocation.get(0).getLongitude();
                    List<Location> locations=new ArrayList<>();
                    locations.add(0,new Location(latitude, longitude));
                    offerLocationEnvelope
                            .data(finalOfferId + "#" + latitude + "," + longitude, OfferLocation.RESOURCE_TYPE)
                            .attr("offer_id",
                                    finalOfferId)
                            .attr("locations",locations );
                    return ResponseEntity.status(HttpStatus.OK).body(offerLocationEnvelope);
                } else {
                    String errDetail = "The Offer is found and valid but no nearby site can be found where it may be accessed.";
                    offerLocationEnvelope.error(ErrorCodes.NO_CONTENT, "The Offer is found and valid but no nearby site can be found where it may be accessed. ", errDetail);
                    return ResponseEntity.status(HttpStatus.NO_CONTENT).body(offerLocationEnvelope);
                }



    }


}