/*
 * Copyright (c) 2018. OneVoice / Arrk Group
 * All rights reserved
 */

package com.onevoice.nux.offerLocation.exception;

public class ServiceException extends Exception {

    private int status;
    private String reason;

    public ServiceException(int status, String reason) {
        this.status = status;
        this.reason = reason;
    }

    public int getStatus() {
        return status;
    }

    public String getReason() {
        return reason;
    }
}
