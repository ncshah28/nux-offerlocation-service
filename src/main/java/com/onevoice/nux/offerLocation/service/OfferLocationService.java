package com.onevoice.nux.offerLocation.service;

import com.onevoice.nux.offerLocation.domain.internal.OfferLocation;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by nidhis on 11/04/2018.
 */
public interface OfferLocationService {

    List<OfferLocation> getListOfLocations();

    List<OfferLocation> getListOfFilteredOffers(String offerId);

    List<OfferLocation> getListOfNearestLocation(String offerId, BigDecimal userLat, BigDecimal userLong);




}
