package com.onevoice.nux.offerLocation.service.impl;

import com.amazonaws.services.s3.AmazonS3;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onevoice.nux.offerLocation.Util.AWSUtil;
import com.onevoice.nux.offerLocation.Util.LocationUtil;

import com.onevoice.nux.offerLocation.domain.external.Location;
import com.onevoice.nux.offerLocation.domain.internal.OfferLocation;
import com.onevoice.nux.offerLocation.service.OfferLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.stream.Collectors;

/**
 * Created by nidhis on 11/04/2018.
 */
@Service
@PropertySource("classpath:aws.properties")
public class OfferLocationS3Impl implements OfferLocationService {

    Location location;

    @Autowired
    Environment env;

    public OfferLocationS3Impl(Location l) {
        this.location=l;
    }

    public OfferLocationS3Impl() {
    }

    @Override
    public List<OfferLocation> getListOfLocations() {
        List<OfferLocation> offerLocations=new ArrayList<>();
        try {
            AmazonS3 s3client= AWSUtil.credentials();
            S3Object s3object = s3client.getObject("nux-ea", "Location.json");
            S3ObjectInputStream inputStream = s3object.getObjectContent();

            InputStream is = s3object.getObjectContent();

                String data = new String(IOUtils.toByteArray(is));
            String list = data.replaceAll("[^\\p{ASCII}]", "");

                ObjectMapper mapper = new ObjectMapper();

                OfferLocation[] obj = mapper.readValue(list, OfferLocation[].class);
            offerLocations= Arrays.asList(obj);

        } catch (Exception e) {

            e.printStackTrace();

        }

        return offerLocations;
    }

    @Override
    public List<OfferLocation> getListOfFilteredOffers(String offerId) {
        List<OfferLocation> listOfLocation =getListOfLocations();

        List<OfferLocation> loc=listOfLocation.stream().filter(offerLocations -> offerLocations.getOfferId().equals(offerId)).collect(Collectors.toCollection(ArrayList::new));
        return loc;
    }

    @Override
    public List<OfferLocation> getListOfNearestLocation(String offerId, BigDecimal userLat, BigDecimal userLong) {

        List<OfferLocation> offerLocations=new ArrayList<>();
       List<OfferLocation> list=getListOfFilteredOffers(offerId);

        if(list!=null && !list.isEmpty())
        {
            OfferLocation finalList=list.stream().map(lists->LocationUtil.calculateDistance(lists,userLat,userLong,"M")).reduce(LocationUtil::min).get();

        offerLocations.add(finalList);}
        return offerLocations;
    }




}

