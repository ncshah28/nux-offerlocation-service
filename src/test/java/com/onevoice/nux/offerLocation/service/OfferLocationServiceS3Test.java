package com.onevoice.nux.offerLocation.service;

import com.onevoice.nux.offerLocation.domain.internal.OfferLocation;
import com.onevoice.nux.offerLocation.service.impl.OfferLocationS3Impl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by nidhis on 18/04/2018.
 */
public class OfferLocationServiceS3Test {

    private OfferLocationService mockOfferLocationService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockOfferLocationService = new OfferLocationS3Impl();
    }

    @Test
    public void testGetListOfLocations() {

    List<OfferLocation> offerLocations=mockOfferLocationService.getListOfLocations();
        assertThat(offerLocations).isNotNull();
    }

    @Test
    public void testGetListOfFilteredOffers() {

        List<OfferLocation> offerLocations=mockOfferLocationService.getListOfFilteredOffers("2");
        assertThat(offerLocations).isNotNull();
    }

    @Test
    public void testGetListOfNearestLocation() {

        List<OfferLocation> offerLocations=mockOfferLocationService.getListOfNearestLocation("2",new BigDecimal(51.5639),new BigDecimal(0.5896));
        assertThat(offerLocations).isNotNull();
    }


}
