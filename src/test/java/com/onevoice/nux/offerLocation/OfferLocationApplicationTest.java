/*
 * Copyright (c) 2018. OneVoice / Arrk Group
 * All rights reserved
 */

package com.onevoice.nux.offerLocation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OfferLocationApplicationTest {

    @Test
    public void contextLoads() {
    }

}
